require 'cigri-clusterlib'
require 'json'

class Controller
  # We try to give a generic API for the controller
  # Here parameters contains most likely a list of all the parameters to use
  # in the case of a PI controller is will contain two values: the coef Kp and Ki
  # It is possible to add others attributes:
  #   - the cumulated error
  #   - some constant matrices
  def initialize(logfile, cluster, config_file)
    config_data = JSON.parse(File.read(config_file))
    # Getting the values from the config
    @nb_jobs = config_data["nb_jobs"].nil? ? 0 : config_data["nb_jobs"]

    @kp = config_data["kp"].nil? ? 0.1 : config_data["kp"].to_f
    @ref = config_data["reference"].nil? ? 3.0 : config_data["reference"].to_f
    # @final_ref = config_data["final_ref"].nil? ? 3.0 : config_data["final_ref"].to_f
    # @init_ref = config_data["init_ref"].nil? ? 0.0 : config_data["init_ref"].to_f
    @gain_ref = config_data["beta"].nil? ? 0.61 : config_data["beta"].to_f # 0.61 = exp(- 30 / 60)
    @alpha = config_data["alpha"].nil? ? 1.0 : config_data["alpha"].to_f
    @constant = config_data["constant"].nil? ? false : config_data["constant"]


    # @previous_ref = 0.0
    # @current_ref = @init_ref
    @previous_y = 0.0
    @previous_smoothed_y = 0.0
    @F = 0.0
    @N = 8.0
    @dt = 30.0

    @error = 0.0

    @sleep_time = config_data["sleep_time"].nil? ? -1 : config_data["sleep_time"].to_i
    @filesize = config_data["file_size"].nil? ? -1 : config_data["file_size"].to_i

    @off = config_data["off"].nil? ? false : true

    @logfile = logfile
    @cluster = cluster

    @io_info = {}
  end

  def is_on()
    !@off
  end

  def is_off()
    @off
  end

  def update_controlled_value()
    if !@constant then
      current_y = @error + @ref
      current_smoothed_y = @gain_ref * @previous_smoothed_y + (1.0 - @gain_ref) * current_y / @N
      
      # @F = ((current_y - @previous_y) / @N) / @dt - @alpha * @nb_jobs
      @F = (current_smoothed_y - @previous_smoothed_y) / @dt - @alpha * @nb_jobs
      # @nb_jobs = bound_jobs(-(@F - (@current_ref - @previous_ref) / @N + @kp * @error / @N) / @alpha)
      @nb_jobs = bound_jobs(-(@F + @kp * @error / @N) / @alpha)
      @previous_smoothed_y = current_smoothed_y
      # @previous_ref = @current_ref
      # @current_ref = @gain_ref * @current_ref + (1.0 - @gain_ref) * @final_ref
    end
  end

  def update_error(value)
    @error = value - @ref
  end

  def get_io_avg()
    @io_info = @cluster.update_io_info(@io_info)
    print("io info: #{@io_info}\n")
    bytes = @io_info.values
    s = 0
    for i in 0..bytes.length - 1
      s = s + bytes[i]
    end
    s = s / bytes.length
    print("avg io: #{s}\n")
    return s
# 
# 
#     bytes = {}
#     campaign_ids = @cluster.running_campaigns()
#     campaign_ids.each {|id|
#       bytes[id] = @cluster.average_io(id)
#     }
#     print("#{bytes}\n")
#     bytes
  end


  def header_log()
    file = File.open(@logfile, "a+")
    file << "time, nb_jobs, waiting, running, load, ref, kp, alpha, beta, sleep_time, filesize, F\n"
    file.close
  end

  def log()
	file = File.open(@logfile, "a+")
    # file << "#{Time.now.to_i}, #{@nb_jobs}, #{self.get_waiting_jobs()}, #{self.read_busy_resources_sensors}\n"
    file << "#{Time.now.to_i}, #{@nb_jobs}, #{self.get_waiting_jobs()}, #{self.get_running_jobs()}, #{self.get_fileserver_load}, #{@ref}, #{@kp}, #{@alpha}, #{@gain_ref}, #{@sleep_time}, #{@filesize}, #{@F}\n"
    file.close
  end

  def get_running_jobs()
	cluster_jobs = @cluster.get_jobs()
    # cluster_jobs.select{|j| j["state"] == "Running" or j["state"] == "Finishing" or j["state"] == "Launching"}.map{|j| j["nodes"].length}.sum
    cluster_jobs.select{|j| j["state"] == "Running" or j["state"] == "Finishing" or j["state"] == "Launching"}.length
  end

  def read_busy_resources_sensors()
    busy_resources_cluster = []
    Dir.glob("/tmp/busy_resources_cluster[0-9]").sort().each_with_index do |f, i|
      l =  `tail -n 1 #{f}`.split()
       busy_resources_cluster[i] = {:date => l[0], :busy => l[1].to_f}
    end
    return busy_resources_cluster[0][:busy]
  end


  def get_waiting_jobs()
	cluster_jobs = @cluster.get_jobs()
    cluster_jobs.select{|j| j["state"] == "Waiting"}.length
  end

  def get_cluster_load()
	@cluster.get_global_stress_factor
  end

  def get_fileserver_load()
      loadavg_per_sensor = []
      Dir.glob("/tmp/loadavg_storage_server[0-9]").sort().each_with_index do |f, i|
	l =  `tail -n 1 #{f}`.split()
	loadavg_per_sensor[i] = {:date => l[0], :mn1 => l[1].to_f, :mn5 => l[2].to_f, :mn15 => l[3].to_f}
      end
      loadavg_per_sensor[0][:mn1]
  end

  def get_error()
	@error
  end

  def get_nb_jobs()
	@nb_jobs
  end

end

def bound_jobs(x)
  if x < 0 then
    0
  else
    x
  end
end
