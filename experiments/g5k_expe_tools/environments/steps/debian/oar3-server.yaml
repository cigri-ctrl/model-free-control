# Install a all-in-one OAR with:
#   - a server
#   - a frontend
#   - a local postgresql database
#
#   *Warning*: As it use systemd to enable the services it will not work
#   out of the box with a simple docker image

- install_dependencies:
  # Dependencies  
  - exec_in: |
      apt-get install -y postgresql apache2-suexec-custom apache2-utils libapache2-mod-fcgid libapache2-mod-php7.0 libapache2-mod-wsgi-py3
  # Dependencies for oar-database (perl version from OAR2)    
  - exec_in: |
      apt-get install -y libsort-versions-perl libterm-ui-perl

- install_oar_server:
  - exec_in: |
      cd oar3-master
      make server-build server-install server-setup
      make user-build
      make user-install drawgantt-svg-install monika-install www-conf-install api-install
      make user-setup drawgantt-svg-setup monika-setup www-conf-setup api-setup
      
- install_init_scripts:
  - exec_in: |
      # Copy initd scripts
      if [ -f /usr/local/share/oar/oar-server/init.d/oar-server ]; then
      cat /usr/local/share/oar/oar-server/init.d/oar-server > /etc/init.d/oar-server
      chmod +x  /etc/init.d/oar-server
      fi

  - exec_in: |
      if [ -f /usr/local/share/oar/oar-server/default/oar-server ]; then
      cat /usr/local/share/oar/oar-server/default/oar-server > /etc/default/oar-server
      fi

      
- configure_database:
  - exec_in: |
      sed -i \
         -e 's/^\(DB_TYPE\)=.*/\1="Pg"/' \
         -e 's/^\(DB_HOSTNAME\)=.*/\1="localhost"/' \
         -e 's/^\(DB_PORT\)=.*/\1="5432"/' \
         -e 's/^\(DB_BASE_PASSWD\)=.*/\1="oar"/' \
         -e 's/^\(DB_BASE_LOGIN\)=.*/\1="oar"/' \
         -e 's/^\(DB_BASE_PASSWD_RO\)=.*/\1="oar_ro"/' \
         -e 's/^\(DB_BASE_LOGIN_RO\)=.*/\1="oar_ro"/' \
         -e 's/^\(SERVER_HOSTNAME\)=.*/\1="localhost"/' \
         -e 's/^\(SERVER_PORT\)=.*/\1="16666"/' \
         -e 's/^\(LOG_LEVEL\)\=\"2\"/\1\=\"3\"/' \
         -e 's#^\(LOG_FILE\)\=.*#\1="/var/log/oar.log"#' \
         -e 's/^\(JOB_RESOURCE_MANAGER_PROPERTY_DB_FIELD\=\"cpuset\".*\)/#\1/' \
         -e 's/^#\(CPUSET_PATH\=\"\/oar\".*\)/\1/' \
         -e 's/^\(FINAUD_FREQUENCY\)\=.*/\1="0"/' \
         /etc/oar/oar.conf

- create_database:
  - exec_in: systemctl start postgresql
  - exec_in: oar-database --create --db-is-local

- configure_restful_api_for_apache2:
  - exec_in: |
      rm -f /etc/oar/api-users
      htpasswd -c -b /etc/oar/api-users oar oar

  - exec_in: |
      sed -i -e '1s@^/var/www.*@/usr/lib/cgi-bin@' /etc/apache2/suexec/www-data
  - exec_in: |
      a2enmod suexec
      a2enmod headers
      a2enmod rewrite

  # Set oar-restful-api configuration (oarapi-unsecure is used for Cigri)
  - exec_in: |
      cat > /etc/oar/apache2/oar-restful-api.conf <<EOF
      #Virtual host to isolate the oar-restful-api (suexec) setup
      Listen 6668
      <VirtualHost *:6668>

      # Aliases to the API.
      WSGIDaemonProcess oarapi user=oar group=oar threads=5
      WSGIScriptAlias /oarapi /usr/local/lib/cgi-bin/oarapi/oarapi.wsgi  
      WSGIScriptAlias /oarapi-public /usr/local/lib/cgi-bin/oarapi/oarapi.wsgi
      WSGIScriptAlias /oarapi-priv /usr/local/lib/cgi-bin/oarapi/oarapi.wsgi
       
      # Suexec configuration
      <IfModule mod_suexec.c>
      SuexecUserGroup oar oar
      </IfModule>

      # Default options for the oar api
      <Directory /usr/local/lib/cgi-bin/oarapi>    
      SetEnv OARCONFFILE /etc/oar/oar.conf
      WSGIProcessGroup oarapi
      WSGIApplicationGroup %{GLOBAL}
      Require all granted
      </Directory>

      
      <IfModule mod_suexec.c>
      SuexecUserGroup oar oar
      </IfModule>
            
      # Set the X_API_PATH_PREFIX variable to value of the header of the same name
      <IfModule rewrite_module>
      RewriteEngine On
      RewriteCond %{HTTP:X_API_PATH_PREFIX}  (.*)
      RewriteRule .* - [E=X_API_PATH_PREFIX:%1]
      </IfModule>
    
      <Location /oarapi-public>
      <IfModule headers_module>
      RequestHeader unset X_REMOTE_IDENT
      </IfModule>
      </Location>
      
      <Location /oarapi>
      <IfModule !ident_module>
      <IfModule headers_module>
      RequestHeader unset X_REMOTE_IDENT
      </IfModule>
      </IfModule>
      <IfModule ident_module>
      IdentityCheck On
      <IfModule headers_module>
      # Set the X_REMOTE_IDENT http header and variable to REMOTE_IDENT env value
      <IfModule rewrite_module>
      RewriteEngine On  
      RewriteCond %{REMOTE_IDENT} (.*)
      RewriteRule .* - [E=X_REMOTE_IDENT:%1]
      RequestHeader add X_REMOTE_IDENT %{X_REMOTE_IDENT}e
      </IfModule>
      </IfModule>
      </IfModule>
      </Location>
      
      <Location /oarapi-priv>                                                 
      Options +ExecCGI -MultiViews +FollowSymLinks                        
      AuthType      basic                                                 
      AuthUserfile  /etc/oar/api-users                                    
      AuthName      "OAR API authentication"                              
      Require valid-user                                                  
      RewriteEngine On                                                    
      RewriteCond %{REMOTE_USER} (.*)                                     
      RewriteRule .* - [E=X_REMOTE_IDENT:%1]                              
      RequestHeader add X_REMOTE_IDENT %{X_REMOTE_IDENT}e                 
      </Location>
      </virtualhost>   
      EOF
  # /var/local/apache2 is missing after image building, must be created at each boot 
  - exec_in: |
      cat > /etc/rc.local <<EOF
      #!/bin/bash    
      mkdir -p /var/log/apache2
      EOF

  - exec_in: chmod +x /etc/rc.local

- activate_oar_restful_api:
  - exec_in: a2enconf oar-restful-api

- enable_services:
  - exec_in: systemctl enable oar-server
  - exec_in: systemctl enable postgresql
