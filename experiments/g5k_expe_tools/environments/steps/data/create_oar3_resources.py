#!/usr/bin/env python3
import sys
from oar.lib import db, Resource

hostnames = [hostname.split('.')[0] for hostname in sys.argv[2:]]
# Open session
db.session()
for h in hostnames:
    for i in range(int(sys.argv[1])):
        print("Create resource network_address: {} core: {}".format(h,i)) 
        Resource.create(network_address=h, core=str(i))
