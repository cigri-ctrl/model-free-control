First, import this repository on your home, if it is not done already:

```
git clone git@gitlab.inria.fr:evalys/big-data-hpc-g5k-expe-tools.git ~/expe_tools

```
Then, get a node on Grod'5000 on any site in interactive mode:

On the frontend:
```sh
oarsub -I
```

Now that you are connected to a node, you have to install install Kameleon
and the tools needed to run the recipe:
```sh
sudo-g5k gem install kameleon-builder
sudo DEBIAN_FRONTEND=noninteractive apt -y install qemu socat pigz polipo libguestfs-tools
```

Go on the repository environment folder and edit it:
```sh
cd ./expe_tools/environments
vim big-data-hpc-slave.yaml
```

Add your G5k user name, the needed version of each tools and so on, and
select the tools you need by commenting the installation steps in the
recipe:
```yaml
  - hadoop
  # I Don't need Spark
  #- spark
  - oar-node
```

Then, build the image:
```sh
kameleon build big-data-hpc-slave.yaml -b /tmp
```

To register the environment on the frontend use the commande provided at
the end of the recipe execution **but on the frontend**, i.e.:
```sh
kaenv3 -a /home/YOUR_G5K_USER/my_g5k_images/big-data-hpc-slave_a18cc5729a5f.yaml
```

Then run the experiments using the scripts provided in each experiments
folder.
