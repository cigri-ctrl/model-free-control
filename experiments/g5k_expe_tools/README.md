# Resource Management Grid'5000 experiment tools

These tools where made originally for the Bebida project by Michael Mercier:
https://gitlab.inria.fr/mmercier/bebida

This is a set of python functions that uses Execo library to reserve
resources on Grid'5000, deploy OS images, configure and start/stop a set
of daemons, etc. for HPC and Big Data resource management experiments.

## Disclaimer

**WARNING**: Because these fonctions where made for a very specific project
they **MUST** be adapted to your project: Please don't use it without
reading the code.

Don't hesitate to contribute by adding more tools support or make these
scripts more generic. Merge requests are welcome :)

## Features

List of supported tools (not exhaustive):
- Spark Standalone and on Hadoop
- Flink
- Thrift
- Hadoop YARN
- HDFS
- Hbase
- Casandra
- BigDataBench
- OAR
- Slurm (not tested)
- Nas Parallel Benchmarks
- ...

It also provide function to:
- launch OAR jobs
- run Spark applications
- generate BigDataBench datasets
- mount storage5k
- gather nodes informations
- ...

## Requirements

You need to install python and the Execo library (already presents on the
Grid'5000 frontends)

Most of the functions are assuming that your nodes have an image deployed
that already contains the tools installed (but not configured).

To be sur that you got everything needed and to achieve a good
reproducibility it is recommended to build your own system image using
Kameleon. To do so, follow the steps explained in this [./environments/README.md](procedure).

## Usage

There is no packaging for this right now.

A simple way to use the ``expe_tools.py`` is a relative import, for example if the your code is in the upper directory:
```python
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '..')))

import expe_tools
```

A lots of examples can be found in the
[Bebida project](https://gitlab.inria.fr/mmercier/bebida). The more
extensive one is this script:
[run_bebida_experiments.py](https://gitlab.inria.fr/mmercier/bebida/blob/master/experiments/run_bebida/run_bebida_experiments.py).
