"""
This script is running a complete OAR cluster deployment on Grid'5000 to
experiment OAR scheduling policies in real conditions.

It use the development version of an OAR scheduler name Kamelot

It give as an results information about scheduling that was done.
"""
import os
import sys
from shutil import copy
from execo import Process, SshProcess
from execo_g5k import oardel
from execo_engine import Engine, logger, ParamSweeper, sweep

sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '../..')))

import expe_tools
from expe_tools import (get_resources, deploy_images, configure_OAR, configure_start_CIGRI,
                        load_experiment_config, gather_nodes_information, save_nodes_role,
                        configure_simple_storages_CIGRI, configure_busy_resources_cluster_CIGRI)

script_path = os.path.dirname(os.path.realpath(__file__))

is_a_test = False
reservation_job_id = None

class ReplayHPCworkload(Engine):
    def __init__(self):
        super(ReplayHPCworkload, self).__init__()
        parser = self.args_parser
        parser.add_argument('--is_a_test',
                            dest='is_a_test',
                            action='store_true',
                            default=False,
                            help='prefix the result folder with "test", enter '
                            'a debug mode if fails and remove the job '
                            'afterward, unless it is a reservation')
        parser.add_argument('--force_reconfig',
                            dest='force_reconfig',
                            action='store_true',
                            default=False,
                            help='if set and oar_job_id is set, the cluster'
                            'is re-configured')
        parser.add_argument('--force_redeploy',
                            help='can be all, master, slaves')
        parser.add_argument('--oar_job_id',
                            help="Grid'5000 reservation job ID")
        parser.add_argument('--oar_inner_job_id',
                            help="Grid'5000 container to schedule in job ID")
        parser.add_argument('experiment_config',
                            help='The config YAML experiment description file')

    def setup_result_dir(self):
        self.result_dir = expe_tools.setup_result_dir(is_a_test)

    def run(self):
        # get CLI paramerters
        oar_job_id = int(self.args.oar_job_id) \
            if self.args.oar_job_id is not None else None
        oar_inner_job_id = int(self.args.oar_inner_job_id) \
            if self.args.oar_inner_job_id is not None else None
        is_a_test = self.args.is_a_test
        force_reconfig = self.args.force_reconfig

        if is_a_test:
            logger.warn('THIS IS A TEST! This run will use only a few '
                        'resources')

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        # get configuration
        config = load_experiment_config(self.args.experiment_config, self.result_dir)
        site = config["grid5000_site"]
        # FIXME resource and job ID are not used and can be wrong if the
        # oar_job_id is set
        walltime = str(config["walltime"])
        additional_nodes = 0
        if "additional_nodes" in config:
            additional_nodes = config["additional_nodes"]
        oar_node_env = config["oar_node_env"]
        oar_cigri_env = config["oar_cigri_env"]
        properties = None
        if "properties" in config:
            properties = config["properties"]
        clusters_conf = config["clusters"]

        nb_storage_servers = 0
        if "nb_storage_servers" in config:
            nb_storage_servers = config['nb_storage_servers']
        
        nb_servers = 1 + nb_storage_servers
        nb_oar_servers = 0
        nb_oar_nodes = 0
        nb_resources = 0
        nb_nodes = additional_nodes + nb_servers 
        nb_resources_clusters = []
        nb_resources_per_node = []
        partitions_distributions = []
        for i, cluster in enumerate(clusters_conf):
            nb_oar_servers += 1
            nb_nodes += 1 + cluster["nb_nodes"]
            nb_oar_nodes += cluster["nb_nodes"]
            nb_res = cluster["nb_resources_per_node"] * cluster["nb_nodes"]
            nb_resources += nb_res
            nb_resources_clusters.append(nb_res)
            nb_resources_per_node.append(cluster["nb_resources_per_node"]) 
            logger.info('Cluster: {}  Nb nodes: {} Nb resources per node: {} Nb resources: {}'.\
                        format(i, cluster["nb_nodes"], cluster["nb_resources_per_node"], nb_res))
            if "partitions" in cluster:
                partitions_distributions.append(cluster["partitions"])
            else:
                partitions_distributions.append(None)

        nb_servers += nb_oar_servers
        logger.info("Nb Asked Nodes: {} Nb OAR servers {}".format(nb_nodes, nb_oar_servers))
        logger.info("Nb Storage servers {} Nb OAR nodes {}".format(nb_storage_servers, nb_oar_nodes))
        
        # workloads = [os.path.expandvars(wf_name) for wf_name in config["workloads"]]
        # # check if workloads exists (Suppose that the same NFS mount point
        # # is present on the remote and the local environment
        # for workload_file in workloads:
        #     with open(workload_file):
        #         pass
        #     # copy the workloads files to the results dir
        #     copy(workload_file, self.result_dir)

        # # define the workloads parameters
        # self.parameters = {
        #     'workload_filename': workloads
        # }
        # logger.info('Workloads: {}'.format(workloads))

        # # define the iterator over the parameters combinations
        # self.sweeper = ParamSweeper(os.path.join(self.result_dir, "sweeps"),
        #                             sweep(self.parameters))

        # # Due to previous (using -c result_dir) run skip some combination
        # logger.info('Skipped parameters:' +
        #             '{}'.format(str(self.sweeper.get_skipped())))

        # logger.info('Number of parameters combinations {}'.format(
        #     str(len(self.sweeper.get_remaining()))))
        # logger.info('combinations {}'.format(
        #     str(self.sweeper.get_remaining())))

        options = "--name={}".format(self.run_name)
        if oar_inner_job_id:
            options = "{} -t inner={}".format(options, oar_inner_job_id)
        if properties:
            options = options + ' ' + properties
            
        job_id, nodes = get_resources("nodes={}".format(nb_nodes), walltime, site,
                                      oar_job_id=oar_job_id,
                                      options=options)

        # master is the first node of the reservation others are slaves
        oar_cigri_servers = nodes[0:nb_servers]
        cigri_server = nodes[0]
        oar_nodes = nodes[-nb_oar_nodes:]
        oar_servers = nodes[1:1 + nb_oar_servers]
        storage_servers = []
        logger.info("CIGRI server: {}".format(cigri_server))
        logger.info("OAR servers: {}".format(oar_servers))
        if nb_storage_servers:
            storage_servers = nodes[1 + nb_oar_servers: 1 + nb_oar_servers + nb_storage_servers]
            logger.info("Storage servers: {}".format(storage_servers))
            
        # update the nodes with only the selected ones
        nodes = oar_cigri_servers + oar_nodes
        
        # if len(nodes) +  != required_resource_nb:
        #     raise RuntimeError("Number of available nodes {} is not equals to"
        #                        " the number of required resources {}".format(
        #                            len(nodes) + 1, required_resource_nb))

        # Do deployment
        deploy_images(oar_cigri_servers, oar_cigri_env, oar_nodes, oar_node_env,
                      force_redeploy=self.args.force_redeploy,
                      required_resource_nb=nb_nodes)

        # Store node list
        nodes_info = gather_nodes_information(nodes, self.result_dir)

        # Store nodes role
        nodes_role = {}
        nodes_role['cigri_server'] = cigri_server.address
        nodes_role['oar_servers'] = [h.address for h in oar_servers]
        nodes_role['oar_nodes'] = [h.address for h in oar_nodes]
        if storage_servers:
            nodes_role['storage_servers'] = [h.address for h in storage_servers]
        save_nodes_role(nodes_role, self.result_dir)

        # Configuration
        # Create temporary directory
        tmp_dir = self.result_dir + '/tmp'
        Process('mkdir -p ' + tmp_dir).run()

        # Configure RJMSs
        idx_first = nb_servers
        for i,cluster in enumerate(clusters_conf):
            idx_end = idx_first + cluster["nb_nodes"]
            oar_nodes = nodes[idx_first:idx_end] 
            idx_first = idx_end
            configure_OAR(tmp_dir, self.result_dir, oar_cigri_servers[i+1],
                          oar_nodes, nodes_info, False,
                          nb_res_per_node=nb_resources_per_node[i],
                          partitions_distribution=partitions_distributions[i])

        # Configure ans CIGRI RJMS
        configure_start_CIGRI(cigri_server, oar_servers, nb_resources_clusters, self.result_dir)

        if storage_servers:
            if 'remote_sensor_script' in config:
                remote_sensor_script = config['remote_sensor_script']
            else:
                remote_sensor_script = None
            configure_simple_storages_CIGRI(cigri_server, storage_servers, oar_nodes, remote_sensor_script)
        if 'busy_sensor_script' in config:
             busy_sensor_script = config['busy_sensor_script']
             configure_busy_resources_cluster_CIGRI(cigri_server, oar_servers, busy_sensor_script)
             
        #configure_CIGRI(oar_cigri_servers[0], [oar_cigri_servers[1]])
        
        # # Do the replay
        # logger.info('Starting to replay')
        # while len(self.sweeper.get_remaining()) > 0:
        #     combi = self.sweeper.get_next()
        #     workload_file = os.path.basename(combi['workload_filename'])
        #     oar_replay = SshProcess(script_path + "/oar_replay.py " +
        #                             combi['workload_filename'] + " " +
        #                             self.result_dir + "  oar_gant_" +
        #                             workload_file,
        #                             master)
        #     oar_replay.stdout_handlers.append(self.result_dir + '/' +
        #                                       workload_file + '.out')
        #     logger.info("replaying workload: {}".format(combi))
        #     oar_replay.run()
        #     if oar_replay.ok:
        #         logger.info("Replay workload OK: {}".format(combi))
        #         self.sweeper.done(combi)
        #     else:
        #         logger.info("Replay workload NOT OK: {}".format(combi))
        #         self.sweeper.cancel(combi)
        #         raise RuntimeError("error in the OAR replay: Abort!")

        # if oar_inner_job_id is not None:
        #     oardel([(job_id, site)])

if __name__ == "__main__":
    engine = ReplayHPCworkload()
    expe_tools.script_path = script_path
    engine.start()
