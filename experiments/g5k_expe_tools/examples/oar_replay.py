#!/usr/bin/env python3
'''
This script is taking a Batsim profile as input and replay the profile
using the provided commands.
'''
import argparse
import os
import json
import sched
import time
import re
import csv
from subprocess import call, Popen, PIPE
from datetime import datetime, timedelta

command_path = os.path.expandvars('$HOME/big-data-hpc-g5k-expe-tools/examples/')

SWF_ADDITIONAL_TIME=600 # Second added to execution time to compose walltime for SWF traces, useless for OWF traces

class WorkloadTrace(object):
    def __init__(self, file_name, cigri_filtering=False):
        self.cigri_filtering = cigri_filtering
        self.file_extension = file_name.split('.')[-1]
        if self.file_extension not in ['json', 'owf', 'swf']:
            print('Only json|owf|swf extension are supported,  not {}'.format(self.file_extension))
            exit()

        self.workload_name = os.path.splitext(os.path.basename(os.path.expandvars(file_name)))[0]
            
        if self.file_extension == 'json':
            # parse the workload to get jobs
            with open(file_name, "r") as workload_trace_file:
                json_data = json.load(workload_trace_file)
                
            self.json_data = json_data
            assert('profiles' in json_data), ("Invalid input file: It must contains a"
                                              "'profiles' map")            
            self.jobs = json_data['jobs']
            self.profiles = json_data['profiles']
        else:
            self.jobs = []
            for line in open(file_name):
                li = line.strip()
                if not li.startswith("#"):
                    job_fields = line.split()
                    if self.file_extension == 'owf':
                        if not(cigri_filtering and job_fields[15] != 0):
                            jobs.append({
                                'subtime': job_fields[1], 
                                'command': 'sleep {}'.format(int(job_fields[3]) - int(job_fields[2])),
                                'walltime': job_fields[4],
                                'resources': job_fields[5],
                                'workload_job_id': job_fields[0]
                            })
                        else:
                            jobs.append({
                                'subtime': job_fields[1], 
                                'command': 'sleep {}'.format(int(job_fields[3])),
                                'walltime': job_fields[3] + SWF_ADDITIONAL_TIME,
                                'resources': job_fields[4],
                                'workload_job_id': job_fields[0] 
                          })       

    def iterate_jobs(self):            
        if self.file_extension == 'json':
            for job in self.json_data['jobs']:
                yield {
                    'subtime': job['subtime'],
                    'command': self.profiles[job['profile']]['command'],
                    'walltime': job['walltime'],
                    'resources': job['res'],
                    'workload_job_id': job['id']
                }
        else:
            for job in self.jobs:
                yield job


def do_oar_submission(command, walltime, resources, result_dir,
                      workload_job_id):
    hms_time = timedelta(seconds=walltime)
    oar_time = str(hms_time).split('.')[0]
    exports = 'export PATH=$PATH:{}; '.format(command_path)
    oar_options = '--stdout={0}/OAR%jobid%.stdout --stderr={0}/OAR%jobid%.stderr '.format(workload_dir)
    oar_cmd = (exports + '/usr/bin/oarsub ' + oar_options + ' -l \\core=' + str(resources) + ',walltime=' +
               oar_time + ' "' + command + '"')
    print(oar_cmd)
    with Popen(oar_cmd, stdout=PIPE, shell=True) as oar_cmd:
        stdout = oar_cmd.stdout.read().decode()
        find_job = re.search("\\nOAR_JOB_ID=(.*)\\n", stdout)
        if find_job:
            job_id = int(find_job.group(1))
            writer.writerow({"workload_job_id": workload_job_id, "oar_id": job_id})
        else:
            print("JOB_NOT_SUBMITTED_WORKLOAD_JOB_ID: {}".format(workload_job_id))


def get_scheduling_results(results_file, begin, end):
    fmt = '%Y-%m-%d %X'
    oar_gantt_cmd = ('oarstat -J --gantt "' + begin.strftime(fmt) + ',' +
                     end.strftime(fmt) + '" > ' + str(results_file))
    print(oar_gantt_cmd)
    call(oar_gantt_cmd, shell=True)

    with open(results_file, "r") as res_file:
        json_data = json.load(res_file)
    jobs = json_data['jobs']
    jobs_details = {}

    for job_id in jobs:
        with Popen(['oarstat', '-Jfj', job_id], stdout=PIPE) as oarstat:
            jobs_details[job_id] = json.loads(oarstat.stdout.read().decode())[job_id]

    with open(results_file + '.details', "w") as job_file:
        json.dump({"jobs": jobs_details}, job_file, sort_keys=True, indent=4)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Replay Batsim profile using'
                                                 'OAR submitions')
    parser.add_argument('--cigri_filtering',
                        dest='cigri_filtering',                        
                        action='store_true',
                        default=False,
                        help="Flag to filter Cigri's jobs (only applicable for .owf workload trace)")    
    parser.add_argument('trace_file',
                        help='The inputs: Batsim profiles file (.json), Standard Workload Format (.swf) or Oar Workload Format (.owf)')
    parser.add_argument('result_dir',
                        help='The result directory to put the result and find the hostname')
    parser.add_argument('output_dir',
                        help='The output directory. It will be put in the result_dir')

    args = parser.parse_args()

    workload_trace = WorkloadTrace(args.trace_file, args.cigri_filtering) 

    result_dir = args.result_dir
    output_dir = args.output_dir

    # schedule the jobs
    begin = datetime.now()

    scheduler = sched.scheduler(time.time, time.sleep)
    for job in workload_trace.iterate_jobs():
        # get command
        #cmd = profiles[job['profile']]['command']
        #job_id = job['id']
        job['result_dir'] = result_dir,
        scheduler.enter(job['subtime'],
                        0,
                        do_oar_submission,
                        kwargs=job)
        
                        # kwargs={'command': job['cmd'],
                        #         'walltime': job['walltime'],
                        #         'resources': job['res'],
                        #         'result_dir': args.result_dir,
                        #         'batsim_job_id': job['id']})

    # Define the timeout with the bigest walltime
    # biggest_walltime = max([job['walltime'] for job in jobs])
    # print("Timeout to get results after all submission is set to: "
    #       "{}".format(biggest_walltime))

    # prepare output directory
    workload_dir = result_dir + '/' + output_dir
    try:
        os.mkdir(workload_dir)
    except OSError as exc:
        if exc.errno != os.errno.EEXIST:
            raise exc

    # prepare output file
    try:
        csv_file = open(workload_dir + '/job_id_mapping.csv', 'w', newline='')
        writer = csv.DictWriter(csv_file, fieldnames=["batsim_id", "oar_id"])
        writer.writeheader()

        # run the jobs
        scheduler.run()
    finally:
        csv_file.close()

    #end_submission = datetime.now()
    ## wait for all the jobs to finish (but no  more than the max walltime)
    #while datetime.now() - end_submission < timedelta(seconds=biggest_walltime):
    while True:
        process = call("oarstat > {}/oarstat.out".format(workload_dir), shell=True)
        if os.stat("{}/oarstat.out".format(workload_dir)).st_size == 0:
            break
        time.sleep(5)

    end = datetime.now()

    get_scheduling_results(workload_dir + '/gantt.json', begin, end)
