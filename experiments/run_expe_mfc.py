"""
This script is running a complete OAR cluster deployment on Grid'5000 to
experiment OAR scheduling policies in real conditions.

It use the development version of an OAR scheduler name Kamelot

It give as an results information about scheduling that was done.
"""
import os
import sys
import json
import shutil
import time
import yaml
from shutil import copy
from execo import Process, SshProcess, Remote
from execo_g5k import oardel
from execo_engine import Engine, logger, ParamSweeper, sweep

sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), '../..')))

import g5k_expe_tools.expe_tools
from g5k_expe_tools.expe_tools import *
# (get_resources, deploy_images, configure_OAR,
#                                        configure_start_CIGRI,
#                                        load_experiment_config, gather_nodes_information, save_nodes_role,
#                                        configure_simple_storages_CIGRI, configure_busy_resources_cluster_CIGRI)

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

IS_A_TEST = False
RESERVATION_JOB_ID = None
OAR_JOB_ID = None

class ReplayHPCworkload(Engine):
    def __init__(self):
        super(ReplayHPCworkload, self).__init__()
        parser = self.args_parser
        parser.add_argument('--is_a_test',
                            dest='is_a_test',
                            action='store_true',
                            default=False,
                            help='prefix the result folder with "test", enter '
                            'a debug mode if fails and remove the job '
                            'afterward, unless it is a reservation')
        parser.add_argument('--force_reconfig',
                            dest='force_reconfig',
                            action='store_true',
                            default=False,
                            help='if set and oar_job_id is set, the cluster'
                            'is re-configured')
        parser.add_argument('--force_redeploy',
                            help='can be all, master, slaves')
        parser.add_argument('--oar_job_id',
                            help="Grid'5000 reservation job ID")
        parser.add_argument('--oar_inner_job_id',
                            help="Grid'5000 container to schedule in job ID")
        parser.add_argument('experiment_config',
                            help='The config YAML experiment description file')
        parser.add_argument('--ref',
                            help='The Reference value for the controller')
        parser.add_argument('--filesize',
                            help='The size of the files')
        parser.add_argument('--job_duration',
                            help='Duration of the CiGri jobs')
        parser.add_argument('--kp',
                            help='kp for MFC')
        parser.add_argument('--kappa',
                            help='kappa for MFC')
        parser.add_argument('--beta',
                            help='beta for the smoothing')
        parser.add_argument('--nb_jobs',
                            help='nb jobs per campaigns')
        self.result_dir = None
        self.oar_node_env = None
        self.oar_cigri_env = None
        self.nb_nodes = 0
        self.nb_servers = 0
        self.nodes = []
        self.partitions_distributions = []
        self.oar_cigri_servers = []
        self.cigri_server = []
        self.oar_nodes = []
        self.oar_servers = []
        self.storage_servers = []
        self.nodes_role = {}
        self.nb_resources_per_node = []
        self.nb_resources_clusters = []

    def setup_result_dir(self):
        """
        Setup the directory for the results
        """
        self.result_dir = g5k_expe_tools.expe_tools.setup_result_dir(IS_A_TEST)

    def get_nodes(self, config, oar_job_id, oar_inner_job_id):
        """
        Get nodes to deploy onto
        """
        site = config["grid5000_site"]
        walltime = str(config["walltime"])
        additional_nodes = 0
        if "additional_nodes" in config:
            additional_nodes = config["additional_nodes"]
        self.oar_node_env = config["oar_node_env"]
        self.oar_cigri_env = config["oar_cigri_env"]
        properties = None
        if "properties" in config:
            properties = config["properties"]
        clusters_conf = config["clusters"]

        nb_storage_servers = 0
        if "nb_storage_servers" in config:
            nb_storage_servers = config['nb_storage_servers']

        self.nb_servers = 1 + nb_storage_servers
        nb_oar_servers = 0
        nb_oar_nodes = 0
        nb_resources = 0
        self.nb_nodes = additional_nodes + self.nb_servers
        # nb_resources_per_node = []
        for i, cluster in enumerate(clusters_conf):
            nb_oar_servers += 1
            self.nb_nodes += 1 + cluster["nb_nodes"]
            nb_oar_nodes += cluster["nb_nodes"]
            nb_res = cluster["nb_resources_per_node"] * cluster["nb_nodes"]
            nb_resources += nb_res
            self.nb_resources_clusters.append(nb_res)
            self.nb_resources_per_node.append(cluster["nb_resources_per_node"])
            logger.info('Cluster: {}  Nb nodes: {} Nb resources per node: {} Nb resources: {}'.\
                        format(i, cluster["nb_nodes"], cluster["nb_resources_per_node"], nb_res))
            if "partitions" in cluster:
                self.partitions_distributions.append(cluster["partitions"])
            else:
                self.partitions_distributions.append(None)

        self.nb_servers += nb_oar_servers
        logger.info("Nb Asked Nodes: {} Nb OAR servers {}".format(self.nb_nodes, nb_oar_servers))
        logger.info("Nb Storage servers {} Nb OAR nodes {}".format(nb_storage_servers,
                                                                   nb_oar_nodes))

        options = "--name={}".format(self.run_name)
        if oar_inner_job_id:
            options = "{} -t inner={}".format(options, oar_inner_job_id)
        if properties:
            options = options + ' ' + properties

        period = 'day'
        if "period" in config:
            period = config["period"]

        job_id, nodes = get_resources("nodes={}".format(self.nb_nodes), walltime, site,
                                       job_type=['deploy', period],
                                       oar_job_id=oar_job_id,
                                       options=options)

        # master is the first node of the reservation others are slaves
        self.oar_cigri_servers = nodes[0:self.nb_servers]
        self.cigri_server = nodes[0]
        self.oar_nodes = nodes[-nb_oar_nodes:]
        self.oar_servers = nodes[1:1 + nb_oar_servers]
        self.storage_servers = []
        logger.info("CIGRI server: {}".format(self.cigri_server))
        logger.info("OAR servers: {}".format(self.oar_servers))
        if nb_storage_servers:
            self.storage_servers = nodes[1 + nb_oar_servers: 1 + \
                                         nb_oar_servers + \
                                         nb_storage_servers]
            logger.info("Storage servers: {}".format(self.storage_servers))
        # update the nodes with only the selected ones
        self.nodes = self.oar_cigri_servers + self.oar_nodes
        return job_id

    def deploy(self):
        """
        Deply the images to the nodes
        """
        # Do deployment
        deploy_images(self.oar_cigri_servers, self.oar_cigri_env, self.oar_nodes, self.oar_node_env,
                      force_redeploy=self.args.force_redeploy,
                      required_resource_nb=self.nb_nodes)
        # Store nodes role
        self.nodes_role['cigri_server'] = self.cigri_server.address
        self.nodes_role['oar_servers'] = [h.address for h in self.oar_servers]
        self.nodes_role['oar_nodes'] = [h.address for h in self.oar_nodes]
        if self.storage_servers:
            self.nodes_role['storage_servers'] = [h.address for h in self.storage_servers]
        save_nodes_role(self.nodes_role, self.result_dir)

    def configure_rjms(self, config):
        """
        Configure OAR
        """
        # Store node list
        nodes_info = gather_nodes_information(self.nodes, self.result_dir)
        # Create temporary directory
        tmp_dir = self.result_dir + '/tmp'
        Process('mkdir -p ' + tmp_dir).run()
        # Configure RJMSs
        clusters_conf = config["clusters"]
        idx_first = self.nb_servers
        for i, cluster in enumerate(clusters_conf):
            idx_end = idx_first + cluster["nb_nodes"]
            # oar_nodes = self.nodes[idx_first:idx_end]
            idx_first = idx_end
            configure_OAR(tmp_dir, self.result_dir, self.oar_cigri_servers[i+1],
                          self.oar_nodes, nodes_info, False,
                          nb_res_per_node=self.nb_resources_per_node[i],
                          partitions_distribution=self.partitions_distributions[i])

    def configure_fileserver(self, config, nb_resources=1):
        """
        Configure the Fileserver
        """
        if self.storage_servers:
            if 'remote_sensor_script' in config:
                remote_sensor_script = config['remote_sensor_script']
            else:
                remote_sensor_script = None
            configure_simple_storages_CIGRI(self.cigri_server,
                                            self.storage_servers,
                                            self.oar_nodes,
                                            remote_sensor_script)
        if 'busy_sensor_script' in config:
            busy_sensor_script = config['busy_sensor_script']
            configure_busy_resources_cluster_CIGRI(self.cigri_server,
                                                   self.oar_servers,
                                                   busy_sensor_script)
    def restart_cigri(self):
        """
        Restart CiGri
        """
        Remote("/etc/init.d/cigri force-stop",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()
        Remote("systemctl restart cigri",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()

    def copy_cigri_files(self, config, path_src, path_cigri_config, folder_name, folder_io_bytes, wrap_script, path_des="/usr/local/share/cigri"):
        """
        Copy all the files required to the CiGri node
        """
        # command = f"cd {path_src} && git diff master --name-only | grep -e 'lib/' -e 'modules/'"
        # get_files_process = Process(command, shell=True)
        # get_files_process.run()
        # # with get_files_process.stdout_fd as std:
        # files_to_copy = get_files_process.stdout
        # logger.info(f"Files to copy: {files_to_copy}")

        # files_to_copy = files_to_copy.split("\n")

        files_to_copy = [
            "lib/cigri-control.rb",
            "lib/cigri-joblib.rb",
            "lib/cigri-colombolib.rb",
            # "lib/cigri-iolib.rb",
            # "lib/cigri-clusterlib.rb",
            "modules/runner.rb"
        ]

        for file_to_copy in files_to_copy:
            if len(file_to_copy) > 1:
                Remote(f"cp {path_src}/{file_to_copy} {path_des}/{file_to_copy}",
                       [self.cigri_server],
                       connection_params={'user': 'root'}).run()
        # Conf CiGri
        Remote(f"cp {path_cigri_config} /etc/cigri/cigri.conf",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()

        # Log file
        Remote("echo 'LOG_CTRL_FILE=\"/tmp/log.txt\"' >> /etc/cigri/cigri.conf",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()

        ctrl_config_path = f"{folder_name}/ctrl_cigri_config.json"
        ctrl_config = config["ctrl_config"]
        logger.info(f"Controller Config: {ctrl_config}")

        with open(ctrl_config_path, 'w') as json_file:
            json.dump(ctrl_config, json_file)

        Remote(f"echo 'CTRL_CIGRI_CONFIG_FILE=\"{ctrl_config_path}\"' >> /etc/cigri/cigri.conf",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()


        Remote("touch /tmp/log.txt; chmod 777 /tmp/log.txt",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()

        self.restart_cigri()

    def run_garbage_collector_for_fileserver(self, file_sizes):
        """
        Delete all the files that are done writting on the fileserver
        """
        for file_size in file_sizes:
            Remote(f"find /var/nfsroot -size {file_size} -delete",
                   [self.storage_servers[0]],
                   connection_params={'user': 'root'}).run()

    def get_gridstat_data(self, path="/home/qguilloteau/gridstat.json"):
        """
        Returns the number of campaigns CiGri that are terminated
        """
        Remote(f"gridstat -d > {path}",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()
        with open(path, "r") as gridstat_json:
            gridstat_data = json.load(gridstat_json)
        # gridstat_json.close()
        return gridstat_data

    def is_oar_job_running(self, job_id, path="/home/qguilloteau/oarstat.json"):
        """
        Returns either the job is running
        """
        Remote(f"oarstat -j {job_id} -J > {path}",
               [self.oar_servers[0]],
               connection_params={'user': 'root'}).run()
        oarstat_json = open(path, "r")
        oarstat_data = json.load(oarstat_json)
        # oarstat_json.close()
        logger.info(f"oarstat data: {oarstat_data}")
        return len(oarstat_data) > 0 and oarstat_data[0]["state"] != "Terminated"
        # return str(job_id) in oarstat_data

    def build_madbench2(self, config, path_bin):
        """
        build madbench2
        """
        source_folder = config["madbench2"]["source_folder"]
        # Process(f"cd {source_folder}/ && mpicc -D SYSTEM -D COLUMBIA -D IO -o {path_bin} MADbench2.c -lm").run()
        Process(f"cd {source_folder}/ && sh build.sh").run()

    def start_madbench2(self, config, out_file, script_folder, path_bin):
        """
        Start the madbench2 benchmark
        """
        global OAR_JOB_ID
        config_mad = config["madbench2"]

        sleep_time = config_mad["sleep_time"]
        no_procs = config_mad["no_procs"]
        no_pix = config_mad["no_pix"]
        no_bin = config_mad["no_bin"]
        no_gang = config_mad["no_gang"]
        sblocksize = config_mad["sblocksize"]
        fblocksize = config_mad["fblocksize"]
        rmod = config_mad["rmod"]
        wmod = config_mad["wmod"]

        # Generate the shell script
        logger.info("Generating script for Madbench2")
        start_end_log = f"{script_folder}/madbench2_start_end.csv"
        script = f"""
sleep {sleep_time}
start_time=$(date +%s.%N)
cd /mnt/nfs0 && mpirun --mca orte_rsh_agent "oarsh" -mca pml ^ucx -machinefile $OAR_NODEFILE -np {no_procs} {path_bin} {no_pix} {no_bin} {no_gang} {sblocksize} {fblocksize} {rmod} {wmod} &> {out_file}
end_time=$(date +%s.%N)
echo "$start_time, $end_time" > {start_end_log}
"""

        path_script_madbench2 = f"{script_folder}/script_madbench2.sh"
        with open(path_script_madbench2, 'w') as script_madbench2:
            script_madbench2.write(script)

        logger.info(f"Submitting the job for Madbench2, starting in {sleep_time}")
        Remote(f"cd /tmp; oarsub -l core={no_procs},walltime=8 'sh {path_script_madbench2}'",
               [self.oar_servers[0]]).start()

    def start_synth_load(self, path_exec):
        """
        Start synth load
        """
        Remote(f"cd /tmp; oarsub -l core=1,walltime=8 'sh {path_exec}'",
               [self.oar_servers[0]]).start()

    def run(self):
        """
        Run the engine
        """
        # get CLI paramerters
        global OAR_JOB_ID
        logger.info(f"self.args = {self.args}")
        oar_job_id = int(self.args.oar_job_id) \
            if self.args.oar_job_id is not None else None
        oar_inner_job_id = int(self.args.oar_inner_job_id) \
            if self.args.oar_inner_job_id is not None else None
        is_a_test = self.args.is_a_test
        # force_reconfig = self.args.force_reconfig

        config = load_experiment_config(self.args.experiment_config, self.result_dir)

        if self.args.ref is not None:
            config["ctrl_config"]["reference"] = float(self.args.ref)

        if self.args.kp is not None:
            config["ctrl_config"]["kp"] = float(self.args.kp)

        if self.args.kappa is not None:
            config["ctrl_config"]["kp"] = 10 ** float(self.args.kappa)

        if self.args.beta is not None:
            config["ctrl_config"]["beta"] = float(self.args.beta)

        if self.args.filesize is not None:
            config["ctrl_config"]["file_size"] = int(self.args.filesize)
            for campaign in config["campaigns"]:
                campaign["file_size"] = int(self.args.filesize)

        if self.args.nb_jobs is not None:
            config["ctrl_config"]["nb_jobs"] = int(self.args.nb_jobs)
            for campaign in config["campaigns"]:
                campaign["nb_jobs"] = int(self.args.nb_jobs)

        if self.args.job_duration is not None:
            config["ctrl_config"]["sleep_time"] = int(self.args.job_duration)
            for campaign in config["campaigns"]:
                campaign["sleep_time"] = int(self.args.job_duration)

        if is_a_test:
            logger.warn('THIS IS A TEST! This run will use only a few '
                        'resources')

        # make the result folder writable for all
        os.chmod(self.result_dir, 0o777)

        logger.info("Getting the Nodes")
        OAR_JOB_ID = self.get_nodes(config, oar_job_id, oar_inner_job_id)
        logger.info(f"OAR JOB ID: {OAR_JOB_ID}")

        if "base_path_folder" in config:
            base_path_folder = config["base_path_folder"]
            Process(f"mkdir -p {base_path_folder}").run()
        else:
            base_path_folder = "/home/qguilloteau"


        folder_name = f"{base_path_folder}/CiGri_MFC_{OAR_JOB_ID}"
        create_folder(folder_name)
        # shutil.copy(self.args.experiment_config, f"{folder_name}/expe.yaml")
        with open(f"{folder_name}/expe.yaml", "w") as expe_file:
            yaml.dump(config, expe_file)

        logger.info("Deploying...")
        self.deploy()

        logger.info("Configuring RJMS")
        self.configure_rjms(config)

        # Configure ans CIGRI RJMS
        logger.info("Configuring CiGri")
        configure_start_CIGRI(self.cigri_server,
                              self.oar_servers,
                              self.nb_resources_clusters,
                              self.result_dir)

        logger.info("Configuring Fileserver")
        self.configure_fileserver(config, self.nb_resources_clusters[0])

        folder_io = f"{folder_name}/bytes"
        create_folder(folder_io)
        wrap_script = f"{folder_name}/wrap.sh"
        generate_wrap_script(wrap_script, folder_io)
        self.copy_cigri_files(config,
                              "/home/qguilloteau/model-free-control/cigri",
                              "/home/qguilloteau/model-free-control/cigri/cigri.conf",
                              folder_name,
                              folder_io,
                              wrap_script)

        if "madbench2" in config:
            source_madbench2 = config["madbench2"]["source_folder"]
            path_bin_madbench2 = f"{source_madbench2}/MADbench2.x"
            self.build_madbench2(config, path_bin_madbench2)
            self.start_madbench2(config,
                                 f"{folder_name}/output_madbench2.out",
                                 folder_name,
                                 path_bin_madbench2)

        if "synth" in config:
            target_load = config["synth"]["target_load"]
            sleep_time = config["synth"]["sleep_time"]
            nb_iters = config["synth"]["nb_iters"]
            path_synth = f"{folder_name}/synth_exec_file.sh"
            path_log_file = f"{folder_name}/output_synth.out"
            generate_synth_exec_file(target_load,
                                     sleep_time=sleep_time,
                                     log_output=path_log_file, 
                                     nb_iters=nb_iters,
                                     filename=path_synth)
            self.start_synth_load(path_synth)

        file_sizes = []

        for campaign_config in config["campaigns"]:
            campaign = Campaign(campaign_config, base_path=folder_name)
            campaign.generate_exec_file()
            campaign.generate_campaign_file()
            logger.info(f"Submitting Campaign: {campaign.campaign_name}")
            campaign.submit_campaign(self.cigri_server)
            file_sizes.append(f"{campaign.file_size}M")

        time.sleep(100)

        max_iter_constant = 15
        previous_progresses = [-i for i in range(max_iter_constant)]
        iteration = 0
        gridstat_data = self.get_gridstat_data(path=f"{folder_name}/gridstat.json")
        max_sleep_time = max(map(lambda x: x["sleep_time"], config["campaigns"]))
        percentage = 0

        # expe_duration = (max_sleep_time / 30) * sum(int(step["iter"]) for step in config["ctrl_config"]["steps"]) + 100
        # while iteration < expe_duration and\
        # while self.is_oar_job_running(1, path=f"{folder_name}/oarstat.json"):# and \
              # any(map(lambda x: x != previous_progresses[0], previous_progresses)):
        # while any(map(lambda x: x != previous_progresses[0], previous_progresses)) and percentage is not None:
        while ("synth" in config and self.is_oar_job_running(1, path=f"{folder_name}/oarstat.json")) or \
         "synth" not in config and percentage is not None and percentage < 100 and any(map(lambda x: x != previous_progresses[0], previous_progresses)):
            time.sleep(30)
            percentage = get_progress(gridstat_data)
            previous_progresses[iteration % max_iter_constant] = percentage
            iteration += 1
            logger.info(f"Progress: {percentage} %")
            gridstat_data = self.get_gridstat_data(path=f"{folder_name}/gridstat.json")
            # self.run_garbage_collector_for_fileserver(file_sizes)
            if has_events(gridstat_data):
                logger.warning("A CiGri Campaign has some events !")

        assert not self.is_oar_job_running(1, path=f"{folder_name}/oarstat.json")

        # assert percentage is None

        logger.info("Cleaning home dir from tmp files")
        clean_home()

        time.sleep(5)

        if "synth" in config and all(map(lambda x: x == previous_progresses[0], previous_progresses)):
            logger.info(f"There is an error: cleaning")
            remove_folder(folder_name)

            logger.info("Giving back the resources")
            oardel([(OAR_JOB_ID, None)])
            raise Exception("Something Bad happend")
            return 1

        logger.info("Get back the log")
        self.get_back_cigri_log(f"{folder_name}/log.csv")

        zip_archive_name = f"{base_path_folder}/CiGri_MFC_{OAR_JOB_ID}"
        logger.info(f"Zipping the data -> {zip_archive_name}")
        zip_files(zip_archive_name, folder_name)
        remove_folder(folder_name)

        logger.info("Giving back the resources")
        oardel([(OAR_JOB_ID, None)])

    def get_back_cigri_log(self, outpath, src="/tmp/log.txt"):
        """
        Copy the log from CiGri to the frontend
        """
        Remote(f"cp {src} {outpath}",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()

    def get_error_cigri_log(self, outpath, src="/tmp/cigri.log"):
        """
        Copy the log from CiGri to the frontend
        """
        Remote(f"cp {src} {outpath}",
               [self.cigri_server],
               connection_params={'user': 'root'}).run()


def clean_home():
    """
    Clean from all the tmp file the home
    """
    # Process("rm /home/qguilloteau/OAR.*", shell=True).run()
    pass

def get_number_of_running_campaigns(gridstat_data):
    """
    Returns the number of running CiGri campaigns
    """
    return gridstat_data["total"]

def has_events(gridstat_data):
    """
    Return either there are some events in a CiGri campaign
    """
    return any(map(lambda x: x["has_events"], gridstat_data["items"]))

def get_progress(gridstat_data):
    """
    Returns the percentage of jobs executed
    """
    try:
        return sum(map(lambda x: x["finished_jobs"], gridstat_data["items"])) * 100 / sum(map(lambda x: x["total_jobs"], gridstat_data["items"]))
    except:
        return None


def zip_files(name, folder):
    """
    zip the folder
    """
    shutil.make_archive(name, "zip", folder)

def create_folder(folder):
    """
    Create a folder
    """
    if not os.path.exists(folder):
        os.makedirs(folder)

def remove_folder(folder):
    """
    Remove the folder
    """
    shutil.rmtree(folder)

def get_nb_reqs(filesize, target_load):
    """
    returns the number of reqs writing a given filesize
    to reach the target load
    """
    alpha = -0.3999045
    beta1 = 0.0016002
    beta2 = 0.007804
    gamma = 0.0016749

    return int((target_load - (alpha + beta1 * filesize)) / (beta2 + gamma * filesize))

def generate_wrap_script(filename, io_path):
    """
    generate the wrapping script for IO
    """
    content = f"""
#/bin/sh
state_file={io_path}/cigri_fs_state_${{OAR_JOB_ID}}
echo $state_file >> /home/qguilloteau/state_files
touch $state_file
resource=$(cat $OAR_RESOURCE_PROPERTIES_FILE | sed -r 's#.*id( = )(.)([0-9]+)(\\2).*#\\3#')
echo $resource >> /home/qguilloteau/state_files
nfs_client_cigri=/mnt/nfs${{resource}}/
echo \"BEGIN_DATE=$(date +%s.%N)\" >> $state_file
echo \"BYTES_BEFORE=$(cd ${{nfs_client_cigri}}; /usr/sbin/mountstats ${{nfs_client_cigri}} | grep 'client wrote' | sed -r 's#([^0-9]*)([0-9]+)(.*)#\\2#')\" >> $state_file

NFS_CLIENT_CIGRI=$nfs_client_cigri $@
RET=$?

echo \"BYTES_AFTER=$(cd ${{nfs_client_cigri}}; /usr/sbin/mountstats ${{nfs_client_cigri}} | grep 'client wrote' | sed -r 's#([^0-9]*)([0-9]+)(.*)#\\2#')\" >> $state_file
echo \"END_DATE=$(date +%s.%N)\" >> $state_file
exit $RET"""
    with open(filename, "w") as wrap_script:
        wrap_script.write(content)
    os.chmod(filename, 0o777)



def generate_synth_exec_file(target_load,
                             sleep_time,
                             log_output,
                             filesize=50,
                             nb_iters=5,
                             filename="$HOME/synth_exec_file.sh"):
    """
    Returns the string of the exec file
    """
    nb_reqs = get_nb_reqs(filesize, target_load)

    exec_content = f"""
#/bin/sh

write_dd() {{
    dd if=/dev/zero of=//mnt/nfs0/file-nfs-synth-$1-$2 bs={filesize}M count=1 oflag=direct && rm /mnt/nfs0/file-nfs-synth-$1-$2 &
}}

export -f write_dd

sleep {sleep_time}

start_time=$(date +%s)

for i in $(seq 1 {nb_iters})
do
    seq 1 {nb_reqs} | xargs -P {nb_reqs} -I {{}} sh -c "write_dd $i {{}}"
    sleep 30
done
end_time=$(date +%s)
echo "start, end, target_load" > {log_output}
echo "$start_time, $end_time, {target_load}" >> {log_output}
sleep {sleep_time}"""

    with open(filename, "w") as synth_exec_file:
        synth_exec_file.write(exec_content)

class Campaign:
    """
    Class describing a CiGri campaign
    """
    def __init__(self, config, base_path="/home/qguilloteau", heaviness=False):
        self.nb_jobs = config["nb_jobs"]
        self.sleep_time = config["sleep_time"]
        self.file_size = config["file_size"]
        self.campaign_name = f"campaign_{self.nb_jobs}j_{self.sleep_time}s_{self.file_size}M"
        self.exec_file_path = f"{base_path}/{self.campaign_name}.sh"
        self.campaign_file_path = f"{base_path}/{self.campaign_name}.json"
        if "heaviness" in config:
            self.heaviness = config["heaviness"]
        else:
            self.heaviness = False
        # self.heaviness = heaviness

    def generate_exec_file(self):
        """
        Generate the executable for the CiGri Campaigns
        """
        write_command = ""
        if self.file_size != 0:
            write_command = f"dd if=/dev/zero of=//mnt/nfs0/file-nfs-{self.campaign_name}-$2 bs={self.file_size}M count=1 oflag=direct && rm /mnt/nfs0/file-nfs-{self.campaign_name}-$2"
            # write_command = f"dd if=/dev/zero of=${{NFS_CLIENT_CIGRI}}/file-nfs-{self.campaign_name}-$2 bs={self.file_size}M count=1 oflag=direct && rm ${{NFS_CLIENT_CIGRI}}/file-nfs-{self.campaign_name}-$2"
        script = f"""#!/bin/bash
echo $2 > /tmp/$1
sleep {self.sleep_time}
{write_command}"""
        exec_file = open(self.exec_file_path, "w")
        exec_file.write(script)
        exec_file.close()
        Process(f"chmod 777 {self.exec_file_path}").run()

    def generate_campaign_file(self):
        """
        Generate the json campaign file
        """
        params = ",\n\t".join(["\"param{index} {index}\"".format(index = i) for i in range(self.nb_jobs)])
        content = f"""
{{
  "name": "{self.campaign_name}",
  "resources": "resource_id=1",
  "exec_file": "{self.exec_file_path}",
  "exec_directory": "/tmp",
  "heaviness": {str(self.heaviness).lower()},
  "test_mode": "false",
  "clusters": {{
    "cluster_0": {{
      "type": "best-effort",
      "walltime": "300"
    }}
  }},
  "prologue": [
    "touch prologue_works"
  ],
  "params": [
      {params}
  ]
}}
        """
        campaign_file = open(self.campaign_file_path, "w")
        campaign_file.write(content)
        campaign_file.close()

    def submit_campaign(self, cigri_server):
        """
        Submit the campaign to CiGri
        """
        Remote(f"gridsub -f {self.campaign_file_path}",
               [cigri_server]).run()

if __name__ == "__main__":
    ENGINE = ReplayHPCworkload()
    g5k_expe_tools.expe_tools.script_path = SCRIPT_PATH
    # ENGINE.start()
    try:
        ENGINE.start()
    except:
        logger.warning(f"Error: Giving back the resources ({OAR_JOB_ID})")
        clean_home()
        oardel([(OAR_JOB_ID, None)])
        sys.exit(1)
