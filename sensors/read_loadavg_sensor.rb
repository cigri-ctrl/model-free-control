#!/usr/bin/ruby

# Funtion to read the last line of /tmp/loadavg_storage_server[0-9] files
# 
# The content of the files is hh:mm:ss followed by the output of cat /proc/loadavg.
# One file per server located in CiGri server
#
# Example /tmp/loadavg_storage_server0
# 14:14:28 2.49 0.92 0.33 1/590 2971
# 14:14:30 2.49 0.92 0.33 1/590 2977
# 14:14:32 2.29 0.90 0.33 1/590 2982
# 14:14:34 2.29 0.90 0.33 1/590 2988

def read_all_loadavg_per_sensor()
  loadavg_per_sensor = []
  Dir.glob("/tmp/loadavg_storage_server[0-9]").sort().each_with_index do |f, i|
    l =  `tail -n 1 #{f}`.split()
    loadavg_per_sensor[i] = {:date => l[0], :mn1 => l[1].to_f, :mn5 => l[2].to_f, :mn15 => l[3].to_f}
  end
  return loadavg_per_sensor
end

loadavg_sensors =  read_all_loadavg_per_sensor()

loadavg_sensors.each do |loadavg|
  puts loadavg
end  
# You can also just use loadavg_1min_server0 = loadavg_sensor[0][:mn1] to access the 1min loadaverage for first server
