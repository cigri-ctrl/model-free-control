#!/usr/bin/ruby

# Funtion to read the last line of /tmp/loadavg_storage_server[0-9] files
# 
# The content of the files is hh:mm:ss followed by the output of cat /proc/loadavg.
# One file per server located in CiGri server
#
# Example /tmp/loadavg_storage_server0
# 14:14:28 10
# 14:14:30 200

def read_busy_resources_sensors()
  busy_resources_cluster = []
  Dir.glob("/tmp/busy_resources_cluster[0-9]").sort().each_with_index do |f, i|
    l =  `tail -n 1 #{f}`.split()
     busy_resources_cluster[i] = {:date => l[0], :busy => l[1].to_f}
  end
  return busy_resources_cluster
end

busy_resources_cluster_sensors = read_busy_resources_sensors()

busy_resources_cluster_sensors.each do |busy|
  puts busy
end  

